<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the 'api' middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1', 'namespace' => 'Api\v1'], function () {
    Route::get('/states', 'StateController@index');
    Route::get('/categories', 'CategoryController@index');
    Route::get('/ads', 'AdController@index');
    Route::get('/ads/{id}', 'AdController@show');

    Route::post('auth/register', 'Auth\AuthController@register');
});

/*
 * Routes private
 */
Route::group(['prefix' => 'v1', 'namespace' => 'Api\v1', 'middleware' => ['auth:api']], function () {
	/* Users */
	Route::get('auth/me', 'Auth\AuthController@me');
    Route::post('auth/updateProfile', 'Auth\AuthController@updateProfile');

    Route::post('/ads', 'AdController@store');
    Route::post('/ads/{id}/update', 'AdController@update');
});

