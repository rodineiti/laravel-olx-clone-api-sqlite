<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdImage extends Model
{
	protected $fillable = ['url','default'];

	/**
     * @var array
     */
    protected $appends = ['image_url'];

	/**
     * @return string|null
     */
    public function getImageUrlAttribute()
    {
        return url('uploads/ads') . '/' . $this->url;
    }

    public function ad()
    {
    	return $this->belongsTo(Ad::class);
    }
}
