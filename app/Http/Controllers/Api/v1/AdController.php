<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Ad;
use App\User;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->limit ?? 10;

        $ads = Ad::with(['user','category','images'])
            ->filter($request->only(['q','cat','state']))
            ->paginate($limit);

        return response()->json($ads);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('id', $request->user()->id)->first();

        if (!$user) {
            return response()->json([
                'error' => true,
                'message' => 'Opss. Usuário não foi encontrado, favor verifique se esta logado.'
            ]);
        }

        $this->validate($request, [
            'title' => ['required','string','min:5','max:255'],
            'category_id' => ['required','exists:categories,id'],
            'price' => ['required'],
            'negotiable' => ['required'],
            'description' => ['required','max:5000']
        ]);

        $ad = $user->ads()->create([
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'category_id' => $request->category_id,
            'price' => $request->price,
            'negotiable' => $request->negotiable,
            'description' => $request->description,
            'state_user' => $user->state,
        ]);

        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $file) {
                $originalName = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $fileName = date('YmdHis')."_".$originalName;

                if (in_array($extension,['jpg','jpeg','png'])) {
                    Image::make($file->path())->fit(750, 750)
                        ->save(public_path("uploads/ads/") . $fileName);
                    $ad->images()->create([
                        'url' => $fileName
                    ]);
                }
            }
        }

        return response()->json([
            'error' => false,
            'message' => 'Adicionado com sucesso',
            'data' => $ad
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $extra = $request->extra ?? false;

        $ad = Ad::with(['user','category','images'])->where('id', $id)->first();

        if ($extra === 'true') {
            $ad->others = Ad::with(['user','category','images'])
                        ->where('user_id', $ad->user_id)
                        ->where('id', '<>', $ad->id)->get();
        }

        return response()->json($ad);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id', $request->user()->id)->first();

        if (!$user) {
            return response()->json([
                'error' => true,
                'message' => 'Opss. Usuário não foi encontrado, favor verifique se esta logado.'
            ]);
        }

        $ad = Ad::with(['user','category','images'])->where('id', $id)->first();

        $this->validate($request, [
            'title' => ['required','string','min:5','max:255'],
            'category_id' => ['required','exists:categories,id'],
            'price' => ['required'],
            'negotiable' => ['required'],
            'description' => ['required','max:5000']
        ]);

        $ad->update([
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'category_id' => $request->category_id,
            'price' => $request->price,
            'negotiable' => $request->negotiable,
            'description' => $request->description,
            'state_user' => $user->state,
        ]);

        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $file) {
                $originalName = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $fileName = date('YmdHis')."_".$originalName;

                if (in_array($extension,['jpg','jpeg','png'])) {
                    Image::make($file->path())->fit(750, 750)
                        ->save(public_path("uploads/ads/") . $fileName);
                    $ad->images()->create([
                        'url' => $fileName
                    ]);
                }
            }
        }

        return response()->json([
            'error' => false,
            'message' => 'Atualizado com sucesso',
            'data' => $ad
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
