<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => ['required','string','min:5','max:255'],
                'email' => ['required','string','email','unique:users'],
                'password' => ['required','string','min:6','confirmed'],
                'state' => ['required','string']
            ]);

            $data = $request->only('name','email','password','state');

            $user = new User();
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = $data['password'];
            $user->state = $data['state'];
            $user->save();

            $request = Request::create('oauth/token', 'POST', [
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => '2LEi8n6vWThoS1quHrXKBAhCSrEFyDv7DLq4qwlh',
                'username' => $data['email'],
                'password' => $data['password'],
            ]);

            $response = app()->handle($request);

            return response()->json(json_decode($response->getContent()));
        } catch (\Exception $exception) {
            return response()->json(["error" => true, "message" => $exception->getMessage()], 400);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateProfile(Request $request)
    {
        $user = User::where('id', $request->user()->id)->first();

        if (!$user) {
            return response()->json([
                'error' => true,
                'message' => 'Opss. Usuário não foi encontrado, favor verifique se esta logado.'
            ]);
        }

        $this->validate($request, [
            'name' => ['required','string','min:5','max:255'],
            'email' => ['required','string','email','unique:users,email,'.$user->id],
            'state' => ['required','string']
        ]);

        if (isset($request->password) && !is_null($request->password)) {
            $this->validate($request, [
                'password' => ['required','string','min:6','confirmed'],
            ]);
        }

        $user->update($request->all());

        return response()->json([
            'error' => false,
            'data' => $user
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        $user = User::with(['ads' => function($q) {
            $q->with(['category','images']);
        }])->where('id', $request->user()->id)->first();

        if (!$user) {
            return response()->json([
                'error' => true,
                'message' => 'Opss. Usuário não foi encontrado, favor verifique se esta logado.'
            ]);
        }

        return response()->json($user);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        if (Auth::check()) {
            Auth::user()->oauthAccessToken()->delete();
        }

        return response()->json(['status' => 'success']);
    }
}