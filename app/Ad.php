<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $fillable = ['title','category_id','slug','cover','price','description','negotiable','state_user'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function category()
    {
    	return $this->belongsTo(Category::class);
    }

    public function images()
    {
    	return $this->hasMany(AdImage::class);
    }

    public function scopeFilter($query, $filter = [])
    {
        $query->where(function($q) use ($filter){
            if (isset($filter['q'])) {
            	$q->where(function($sq) use ($filter) {
            		$sq->orWhere('title', 'LIKE', "%{$filter['q']}%");
            		$sq->orWhere('description', 'LIKE', "%{$filter['q']}%");
            	});
            }

            if (isset($filter['cat'])) {
            	$category = Category::where('slug', $filter['cat'])->first();
            	$q->where('category_id', $category->id);
            }

            if (isset($filter['state'])) {
            	$q->where('state_user', $filter['state']);
            }
        });
    }
}
