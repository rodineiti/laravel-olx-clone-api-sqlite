<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(array('title' => 'Bebês', 'slug' => 'bebes', 'img' => 'http://alunos.b7web.com.br:501/assets/images/baby.png'));
        Category::create(array('title' => 'Carros', 'slug' => 'carros', 'img' => 'http://alunos.b7web.com.br:501/assets/images/cars.png'));
        Category::create(array('title' => 'Roupas', 'slug' => 'roupas', 'img' => 'http://alunos.b7web.com.br:501/assets/images/clothes.png'));
        Category::create(array('title' => 'Eletrônicos', 'slug' => 'eletronicos', 'img' => 'http://alunos.b7web.com.br:501/assets/images/electronics.png'));
    }
}
