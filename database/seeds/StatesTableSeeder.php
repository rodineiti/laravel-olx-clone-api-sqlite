<?php

use Illuminate\Database\Seeder;
use App\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        State::create(array('description' => 'Acre', 'slug' => 'AC'));
        State::create(array('description' => 'Alagoas', 'slug' => 'AL'));
        State::create(array('description' => 'Amapá', 'slug' => 'AP'));
        State::create(array('description' => 'Amazonas', 'slug' => 'AM'));
        State::create(array('description' => 'Bahia', 'slug' => 'BA'));
        State::create(array('description' => 'Ceará', 'slug' => 'CE'));
        State::create(array('description' => 'Distrito Federal', 'slug' => 'DF'));
        State::create(array('description' => 'Espírito Santo', 'slug' => 'ES'));
        State::create(array('description' => 'Goiás', 'slug' => 'GO'));
        State::create(array('description' => 'Maranhão', 'slug' => 'MA'));
        State::create(array('description' => 'Mato Grosso', 'slug' => 'MT'));
        State::create(array('description' => 'Mato Grosso do Sul', 'slug' => 'MS'));
        State::create(array('description' => 'Minas Gerais', 'slug' => 'MG'));
        State::create(array('description' => 'Pará', 'slug' => 'PA'));
        State::create(array('description' => 'Paraíba', 'slug' => 'PB'));
        State::create(array('description' => 'Paraná', 'slug' => 'PR'));
        State::create(array('description' => 'Pernambuco', 'slug' => 'PE'));
        State::create(array('description' => 'Piauí', 'slug' => 'PI'));
        State::create(array('description' => 'Rio de Janeiro', 'slug' => 'RJ'));
        State::create(array('description' => 'Rio Grande do Norte', 'slug' => 'RN'));
        State::create(array('description' => 'Rio Grande do Sul', 'slug' => 'RS'));
        State::create(array('description' => 'Rondônia', 'slug' => 'RO'));
        State::create(array('description' => 'Roraima', 'slug' => 'RR'));
        State::create(array('description' => 'Santa Catarina', 'slug' => 'SC'));
        State::create(array('description' => 'São Paulo', 'slug' => 'SP'));
        State::create(array('description' => 'Sergipe', 'slug' => 'SE'));
        State::create(array('description' => 'Tocantins', 'slug' => 'TO'));
    }
}
